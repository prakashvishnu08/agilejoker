class RenameGithubRepoToGithubHooks < ActiveRecord::Migration
  def change
    rename_table :github_repos, :github_hooks
  end
end
