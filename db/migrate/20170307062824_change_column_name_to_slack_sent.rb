class ChangeColumnNameToSlackSent < ActiveRecord::Migration
  def change
    rename_column :email_logs, :slack_send, :slack_sent
  end
end
