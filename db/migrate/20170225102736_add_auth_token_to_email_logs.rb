class AddAuthTokenToEmailLogs < ActiveRecord::Migration
  def change
    add_column :email_logs, :auth_token, :string
  end
end
