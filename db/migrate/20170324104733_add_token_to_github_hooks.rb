class AddTokenToGithubHooks < ActiveRecord::Migration
  def change
    add_column :github_hooks, :token, :string
  end
end
