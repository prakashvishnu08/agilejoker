class AddTeamToPosts < ActiveRecord::Migration
  def change
    add_reference :posts, :team, index: true, foreign_key: true
  end
end
