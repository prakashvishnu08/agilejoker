class AddHookIdToGithubHooks < ActiveRecord::Migration
  def change
    add_column :github_hooks, :hook_id, :integer
  end
end
