class AddTimezoneToTeamSetting < ActiveRecord::Migration
  def change
    add_column :team_settings, :timezone, :string
  end
end
