class CreateExtActivities < ActiveRecord::Migration
  def change
    create_table :ext_activities do |t|
      t.string :event_id
      t.string :event_name
      t.string :event_time
      t.datetime :event_time
      t.string :event_url
      t.string :event_object
      t.string :event_object_url
      t.text :message
      t.string :author_name
      t.string :author_login
      t.string :author_avatar_url
      t.string :tool
      t.integer :user_id
      t.integer :team_id

      t.timestamps null: false
    end
  end
end
