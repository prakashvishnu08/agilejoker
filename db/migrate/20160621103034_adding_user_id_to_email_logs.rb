class AddingUserIdToEmailLogs < ActiveRecord::Migration
  def change
    add_reference :email_logs, :user, index: true
  end
end
