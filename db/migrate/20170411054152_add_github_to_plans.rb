class AddGithubToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :github, :boolean, default: false
  end
end
