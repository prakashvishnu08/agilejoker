class AddSlackSendToEmailLogs < ActiveRecord::Migration
  def change
    add_column :email_logs, :slack_send, :boolean
  end
end
