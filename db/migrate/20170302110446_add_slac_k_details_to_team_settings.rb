class AddSlacKDetailsToTeamSettings < ActiveRecord::Migration
  def change
    add_column :team_settings, :slack_enabled, :boolean, default: false
    add_column :team_settings, :slack_token, :string
    add_column :team_settings, :slack_url, :string
  end
end
