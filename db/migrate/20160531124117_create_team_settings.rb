class CreateTeamSettings < ActiveRecord::Migration
  def change
    create_table :team_settings do |t|
      t.datetime :question_email_at
      t.datetime :digest_email_at
      t.belongs_to :team, index: true
      
      t.timestamps null: false
    end
  end
end
