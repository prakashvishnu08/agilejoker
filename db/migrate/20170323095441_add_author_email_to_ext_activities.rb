class AddAuthorEmailToExtActivities < ActiveRecord::Migration
  def change
    add_column :ext_activities, :author_email, :string
  end
end
