class AddGitHubTokenToTeamSettings < ActiveRecord::Migration
  def change
    add_column :team_settings, :github_token, :string
  end
end
