class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.integer :team_id
      t.integer :created_by
      t.integer :tag_type, default: 0

      t.timestamps null: false
    end
  end
end
