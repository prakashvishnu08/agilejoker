class CreateGithubRepos < ActiveRecord::Migration
  def change
    create_table :github_repos do |t|
      t.references :team, index: true, foreign_key: true
      t.string :repo_name
      t.integer :repo_id

      t.timestamps null: false
    end
  end
end
