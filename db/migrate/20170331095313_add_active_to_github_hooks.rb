class AddActiveToGithubHooks < ActiveRecord::Migration
  def change
    add_column :github_hooks, :active, :boolean
  end
end
