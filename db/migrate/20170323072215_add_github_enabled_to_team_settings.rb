class AddGithubEnabledToTeamSettings < ActiveRecord::Migration
  def change
    add_column :team_settings, :github_enabled, :boolean
  end
end
