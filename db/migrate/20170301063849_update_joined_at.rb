class UpdateJoinedAt < ActiveRecord::Migration
  def change
    Allotment.unscoped.all.each do |f|
      unless f.joined_at.present?
        f.update(joined_at: f.created_at)
      end
    end
  end
end
