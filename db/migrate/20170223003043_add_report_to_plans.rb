class AddReportToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :report, :boolean, default: false
  end
end
