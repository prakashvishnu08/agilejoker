class CreateEmailLogs < ActiveRecord::Migration
  def change
    create_table :email_logs do |t|
      t.boolean :question_sent
      t.boolean :digest_sent
      t.datetime :sent_date
      t.belongs_to :team, index: true
      
      t.timestamps null: false
    end
  end
end
