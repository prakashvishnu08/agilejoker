class AddJoinedAtToAllotments < ActiveRecord::Migration
  def change
    add_column :allotments, :joined_at, :datetime
  end
end
