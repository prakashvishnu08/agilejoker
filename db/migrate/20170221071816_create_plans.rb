class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.integer :max_teams
      t.boolean :data_retention
      t.integer :max_users
      t.integer :code
      t.string :name

      t.timestamps null: false
    end
  end
end
