class AddDeletedAtToFields < ActiveRecord::Migration
  def change
    add_column :email_logs, :deleted_at, :datetime
    add_index :email_logs, :deleted_at

    add_column :team_settings, :deleted_at, :datetime
    add_index :team_settings, :deleted_at

    add_column :posts, :deleted_at, :datetime
    add_index :posts, :deleted_at

    add_column :allotments, :deleted_at, :datetime
    add_index :allotments, :deleted_at
  end
end
