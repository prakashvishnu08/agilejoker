class AddRoleToAllotments < ActiveRecord::Migration
  def change
    add_column :allotments, :role, :string, default: 'member'
  end
end
