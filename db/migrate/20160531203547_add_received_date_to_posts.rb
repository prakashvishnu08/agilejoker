class AddReceivedDateToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :received_date, :datetime
  end
end
