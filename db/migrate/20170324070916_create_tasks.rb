class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :title
      t.datetime :due_on
      t.datetime :completed_on
      t.integer :created_by
      t.integer :team_id
      t.boolean :blocker, default: false

      t.timestamps null: false
    end
  end
end
