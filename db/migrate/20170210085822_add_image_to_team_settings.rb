class AddImageToTeamSettings < ActiveRecord::Migration
  def change
    add_column :team_settings, :image, :string
  end
end
