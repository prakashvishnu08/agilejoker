class AddDaysToTeamSettings < ActiveRecord::Migration
  def change
  	add_column :team_settings, :days, :text
  end
end
