class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :subscription_id
      t.integer :status, default: 0, null: false
      t.integer :user_id
      t.decimal :amount
      t.string :paypal_token
      t.string :paypal_payer_id
      t.integer :plan_id
      t.datetime :purchased_at
      t.string :ip

      t.timestamps null: false
    end
  end
end
