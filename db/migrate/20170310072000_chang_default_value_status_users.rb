class ChangDefaultValueStatusUsers < ActiveRecord::Migration
  def change
    change_column :users, :status, :boolean, :default => false
  end
end
