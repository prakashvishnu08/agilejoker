class AddSlackToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :slack, :boolean, default: false
  end
end
