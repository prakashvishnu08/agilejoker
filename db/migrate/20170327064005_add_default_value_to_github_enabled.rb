class AddDefaultValueToGithubEnabled < ActiveRecord::Migration
  def change
    change_column :team_settings, :github_enabled, :boolean, :default => false
  end
end
