class RenameImageToPhoto < ActiveRecord::Migration
  def change
    rename_column :team_settings, :image, :photo
    rename_column :users, :image, :photo
  end
end
