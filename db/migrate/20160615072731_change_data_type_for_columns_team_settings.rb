class ChangeDataTypeForColumnsTeamSettings < ActiveRecord::Migration
  def change
    change_column :team_settings, :question_email_at, :string
    change_column :team_settings, :digest_email_at, :string
  end
end
