class AddPriceToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :price, :decimal, default: 0
  end
end
