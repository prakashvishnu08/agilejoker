# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170411054152) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "allotments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "team_id"
    t.boolean  "accepted",   default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "role",       default: "member"
    t.datetime "joined_at"
    t.datetime "deleted_at"
  end

  add_index "allotments", ["deleted_at"], name: "index_allotments_on_deleted_at", using: :btree
  add_index "allotments", ["team_id"], name: "index_allotments_on_team_id", using: :btree
  add_index "allotments", ["user_id"], name: "index_allotments_on_user_id", using: :btree

  create_table "email_logs", force: :cascade do |t|
    t.boolean  "question_sent"
    t.boolean  "digest_sent"
    t.datetime "sent_date"
    t.integer  "team_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
    t.string   "auth_token"
    t.boolean  "slack_sent"
    t.datetime "deleted_at"
  end

  add_index "email_logs", ["deleted_at"], name: "index_email_logs_on_deleted_at", using: :btree
  add_index "email_logs", ["team_id"], name: "index_email_logs_on_team_id", using: :btree
  add_index "email_logs", ["user_id"], name: "index_email_logs_on_user_id", using: :btree

  create_table "ext_activities", force: :cascade do |t|
    t.string   "event_id"
    t.string   "event_name"
    t.datetime "event_time"
    t.string   "event_url"
    t.string   "event_object"
    t.string   "event_object_url"
    t.text     "message"
    t.string   "author_name"
    t.string   "author_login"
    t.string   "author_avatar_url"
    t.string   "tool"
    t.integer  "user_id"
    t.integer  "team_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "author_email"
  end

  create_table "github_hooks", force: :cascade do |t|
    t.integer  "team_id"
    t.string   "repo_name"
    t.integer  "repo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "token"
    t.boolean  "active"
    t.integer  "hook_id"
  end

  add_index "github_hooks", ["team_id"], name: "index_github_hooks_on_team_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "subscription_id"
    t.integer  "status",          default: 0, null: false
    t.integer  "user_id"
    t.decimal  "amount"
    t.string   "paypal_token"
    t.string   "paypal_payer_id"
    t.integer  "plan_id"
    t.datetime "purchased_at"
    t.string   "ip"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "plans", force: :cascade do |t|
    t.integer  "max_teams"
    t.boolean  "data_retention"
    t.integer  "max_users"
    t.integer  "code"
    t.string   "name"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "report",         default: false
    t.boolean  "slack",          default: false
    t.decimal  "price",          default: 0.0
    t.boolean  "github",         default: false
  end

  create_table "posts", force: :cascade do |t|
    t.text     "today"
    t.text     "tomorrow"
    t.text     "blockers"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.datetime "received_date"
    t.integer  "team_id"
    t.datetime "deleted_at"
  end

  add_index "posts", ["deleted_at"], name: "index_posts_on_deleted_at", using: :btree
  add_index "posts", ["team_id"], name: "index_posts_on_team_id", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "subscriptions", force: :cascade do |t|
    t.boolean  "active"
    t.string   "plan_code"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
  add_index "taggings", ["task_id"], name: "index_taggings_on_task_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.integer  "team_id"
    t.integer  "created_by"
    t.integer  "tag_type",   default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.text     "title"
    t.datetime "due_on"
    t.datetime "completed_on"
    t.integer  "created_by"
    t.integer  "team_id"
    t.boolean  "blocker",      default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "assigned_to"
  end

  create_table "team_settings", force: :cascade do |t|
    t.string   "question_email_at"
    t.string   "digest_email_at"
    t.integer  "team_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "timezone"
    t.string   "photo"
    t.boolean  "slack_enabled",     default: false
    t.string   "slack_token"
    t.string   "slack_url"
    t.datetime "deleted_at"
    t.text     "days"
    t.string   "github_token"
    t.boolean  "github_enabled",    default: false
  end

  add_index "team_settings", ["deleted_at"], name: "index_team_settings_on_deleted_at", using: :btree
  add_index "team_settings", ["team_id"], name: "index_team_settings_on_team_id", using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "created_by"
    t.string   "uid"
    t.datetime "deleted_at"
  end

  add_index "teams", ["deleted_at"], name: "index_teams_on_deleted_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "status",                 default: false
    t.string   "name"
    t.string   "timezone"
    t.string   "photo"
    t.boolean  "super_admin"
    t.string   "github_username"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "github_hooks", "teams"
  add_foreign_key "posts", "teams"
end
