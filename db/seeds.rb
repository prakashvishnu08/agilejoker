# Seeds for weavelabs

u1 = User.create(email: 'awin@weavelabs.com', password: 'password', name: 'Awin Abi')
u1.confirmed_at = Time.current
u1.remote_photo_url = 'https://beta.weavelabs.com/images/team/awin.jpg'
u1.save


u2 = User.create(email: 'charles.skariah@weavelabs.com', password: 'password', name: 'Charles Skariah')
u2.confirmed_at = Time.current
u2.remote_photo_url = 'https://beta.weavelabs.com/images/team/charles.jpg'
u2.save

u3 = User.create(email: 'vinod@weavelabs.com', password: 'password', name: 'Vinod Varghese')
u3.confirmed_at = Time.current
u3.remote_photo_url = 'https://beta.weavelabs.com/images/team/vinod.jpg'
u3.save

u4 = User.create(email: 'vishnu.prakash@weavelabs.com', password: 'password', name: 'Vishuprakash TP')
u4.confirmed_at = Time.current
u4.save

u5 = User.create(email: 'asher.ben@weavelabs.com', password: 'password', name: 'Asher Ben')
u5.confirmed_at = Time.current
u5.save

u6 = User.create(email: 'rohit.george@weavelabs.com', password: 'password', name: 'Rohit George')
u6.confirmed_at = Time.current
u6.save

team = Team.default_team(u1)
team.update(name: 'Weave Labs')
team.team_setting.remote_photo_url = 'https://pbs.twimg.com/profile_images/565169119884881921/iTCTERmI.png'
team.team_setting.save

Plan.create(name: "FREE", max_teams: 1, data_retention: true, max_users: 10, code: 111, report: true )
Plan.create(name: "PRO", max_teams: 100, data_retention: false, max_users: 40, code: 112, report: true, price: 9 )

Allotment.create(team_id: team.id, user_id: u2.id, accepted: true, role: 'member')
Allotment.create(team_id: team.id, user_id: u3.id, accepted: true, role: 'observer')
Allotment.create(team_id: team.id, user_id: u4.id, accepted: true, role: 'member')
Allotment.create(team_id: team.id, user_id: u5.id, accepted: false, role: 'member')
Allotment.create(team_id: team.id, user_id: u6.id, accepted: false, role: 'observer')

SENTANCES = [
  "I think I will buy the red car, or I will lease the blue one.",
  "The clock within this blog and the clock on my laptop are 1 hour different from each other.",
  "If I don’t like something, I’ll stay away from it.",
  "The river stole the gods.",
  "The memory we used to share is no longer coherent.",
  "I would have gotten the promotion, but my attendance wasn’t good enough.",
  "This is a Japanese doll.",
  "Malls are great places to shop; I can find everything I need under one roof.",
  "If you like tuna and tomato sauce- try combining the two. It’s really not as bad as it sounds.",
  "I checked to make sure that he was still alive.",
  "They got there early, and they got really good seats.",
  "Italy is my favorite country; in fact, I plan to spend two weeks there next year."
]

[5, 4, 3, 2, 1].each do |i|
  [u1,u2,u4].each do |user|
    Post.create(
      today: SENTANCES.sample,
      tomorrow: SENTANCES.sample,
      blockers: SENTANCES.sample,
      received_date: i.days.ago,
      team_id: team.id,
      user_id: user.id
    )
  end
end

