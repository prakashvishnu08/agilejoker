class SuperAdminPolicy  < Struct.new(:user, :super_admin)
  attr_reader :user, :admin

  def initialize(user, admin)
    @user = user
    @admin = admin
  end

  def index?
   user.super_admin?
  end
end
