class TeamPolicy
  attr_reader :user, :team

  def initialize(user, team)
    @user = user
    @team = team
  end

  def show?
    team.users.include?user
  end

  def update?
    user.team_admin(team)
  end

  def new?
    if Team.where(created_by: user.id).count >= user.plan.max_teams
      false
    else
      true
    end
  end

  def add_users?
    if team.users.count >= user.plan.max_users
      false
    else
      true
    end
  end
end
