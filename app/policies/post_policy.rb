class PostPolicy
  attr_reader :user, :post

  def initialize(user, post)
    @user = user
    @post = post
  end

  def index?
   user.plan.report?
  end

  def search?
   user.plan.report?
  end
end
