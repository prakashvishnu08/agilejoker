class GithubPayloadJob < ActiveJob::Base
  queue_as :default

  def perform(params)
    team = Team.find(params[:team_id])
    if team.github_hooks.where(hook_id: params[:hook_id]).blank?
      GithubHook.find_by(repo_id: params[:repository][:id]).update(hook_id: params[:hook_id])
    end

    if params[:commits].present?
      event_object = params[:repository][:name]
      event_object_url = params[:repository][:url]

      params[:commits].each do |commit|
        user = User.find_by(email: commit[:author][:email]) || User.find_by(github_username: commit[:author][:username])
        if user.present?
          user_id = user.id
        else
          user_id = nil
        end
        team.ext_activities.create(event_id: commit[:id], event_name: "commit",
          event_time: commit[:timestamp], event_url: commit[:url],event_object: event_object,
          event_object_url: event_object_url, message: commit[:message], author_name: commit[:author][:name],
          author_login: commit[:author][:username], author_email: commit[:author][:email], tool: "github", user_id: user_id)
      end
    end
  end

end
