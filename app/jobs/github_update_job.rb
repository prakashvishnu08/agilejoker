class GithubUpdateJob < ActiveJob::Base
  queue_as :default

  def perform(team, repos, team_settings)
    repo_ids = []
    client = Octokit::Client.new access_token: team_settings.github_token
    if repos.present?
      repos.each do |repo|
        repo_full_name = repo.split(" ").first
        repo_id = repo.split(" ").second
        repo_ids << repo_id
        if !team.github_hooks.where(repo_id: repo_id).present?
          hook = GithubHook.create(repo_name: repo_full_name, repo_id: repo_id, team_id: team.id, active: true)
          GithubHook.create_webhook(client, team.id, repo_full_name, hook.token)

        elsif team.github_hooks.where(repo_id: repo_id, active: false).present?
          hook = team.github_hooks.find_by(repo_id: repo_id)
          GithubHook.create_webhook(client, team.id, repo_full_name, hook.token)
          hook.update(active: true)
        end
      end
    end
    unhook_repos = team.github_hooks.where.not(repo_id: repo_ids)
    if unhook_repos.present?
      unhook_repos.each do |repo|
        status = client.remove_hook(repo.repo_name, repo.hook_id)
        if status == true
          GithubHook.find(repo.id).update(active: false, hook_id: nil)
        end
      end
    end
  end
end
