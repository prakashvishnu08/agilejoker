class CreateGithubHookJob < ActiveJob::Base
  queue_as :default

  def perform(team_id, repos, team_settings)
    client = Octokit::Client.new access_token: team_settings.github_token
    if repos.present?
      repos.each do |repo|
        repo_full_name = repo.split(" ").first
        repo_id = repo.split(" ").second
        hook = GithubHook.create(repo_name: repo_full_name, repo_id: repo_id, team_id: team_id, active: true)
        GithubHook.create_webhook(client, team_id, repo_full_name, hook.token)
      end
    end
  end
end
