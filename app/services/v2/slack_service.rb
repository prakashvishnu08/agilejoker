module V2
  class SlackService
    attr_reader :team
    require 'slack'

    def initialize(team)
      @team = team
    end

    def create_digest_content
      team_digest_time = @team.team_setting.digest_email_at
      time_zone = @team.team_setting.timezone
      team_digest_time_utc = ActiveSupport::TimeZone[time_zone].parse(team_digest_time).in_time_zone('UTC')
      email_log = @team.email_logs.where('slack_sent = ? AND user_id is ? AND date(sent_date) = ?', true, nil, Date.today)
      if !email_log.present? && @team.team_setting.slack_enabled
        if ((DateTime.current - 14.minutes..DateTime.current + 1.minutes).cover?team_digest_time_utc) && (@team.team_setting.days.include?(team_digest_time_utc.wday.to_s))
          slack_date = @team.question_date
          date = slack_date.strftime('%A, %d %b %Y')
          @team.email_logs.create(slack_sent: true, sent_date: DateTime.now)
          content = V2::TaskDigestService.new(@team, slack_date).digest_content
          commits = ExtActivity.commits(@team.id, slack_date).where.not(user_id: nil)
          content[:github_commits] = commits
          slack_content = slack_content(content)
          send_digest_to_slack(slack_content, @team, date)
        end
      end
    end

    def slack_content(content)
      post_array = []
      text_array = []
      if content.present?
        content.each do |key, tasks|
          if tasks.present?
            if (tasks.any? { |x| x.is_a? ExtActivity }) && (@team.team_setting.github_enabled)
              digest = "\n" + "*#{key.to_s.humanize}*" + "\n"
              tasks.group_by(&:user).each do |user, tasks|
                digest = digest + "\n" + "*#{user.name}*" + ' - ' + "*_#{tasks.count} Github Commits_*" + "\n"
              end
            elsif tasks.any? { |x| x.is_a? Task }
              digest = "\n" + "*#{key.to_s.humanize}*" + "\n"
              tasks.each do |task|
                digest = digest + "\n" + "#{task.title}" + ' - ' + "*_#{task.assignee.name}_*" + "\n"
              end
            end
            post_array << digest
          end
        end
        for i in 0..(post_array.count - 1)
          text_array << { 'color' => "#{SecureRandom.hex(3)}", 'text' => "#{post_array[i]}",
           'mrkdwn_in' => ['text'] }
        end
        return text_array
      end
    end

    def send_digest_to_slack(slack_content, team, date)
      if slack_content.blank?
        digest = "It looks like we haven't received any updates from the team members yesterday."
      end
      team_settings = Team.find(team.id).team_setting
      conn = Faraday.new(url: team_settings.slack_url)
      conn.post do |req|
        req.headers['Content-Type'] = 'application/json'
        req.body = {
          'text' => "*Digest for #{date}*\n #{digest}",
          'mrkdwn' => true,
          'attachments' =>
          if slack_content.present?
            slack_content.each do |text|
              [text]
            end
          end
        }.to_json
      end
    end
  end
end
