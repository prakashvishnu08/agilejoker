module V2
  class TaskDigestService
    attr_reader :team, :date

    def initialize(team, date)
      @team = team
      @date = date
    end

    def digest_content
      completed_tasks = Task.includes(:assignee).where("date(completed_on) = ? AND team_id = ?", @date, @team.id)
      todays_tasks = Task.includes(:assignee).where(due_on: Date.tomorrow.in_time_zone(@team.timezone), team_id: @team.id)
      blockers = Task.includes(:assignee).where("team_id = ? AND date(due_on) = ? AND blocker = ?", @team.id, @date, true)
      content = {completed_tasks: completed_tasks, todays_tasks: todays_tasks, blockers: blockers}
    end
  end
end
