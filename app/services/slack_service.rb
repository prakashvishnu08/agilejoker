class SlackService
  attr_reader :team
  require "slack"

  def initialize(team)
    @team = team
  end

  def create_digest_content
    team_digest_time = @team.team_setting.digest_email_at
    time_zone = @team.team_setting.timezone
    team_digest_time_utc = ActiveSupport::TimeZone[time_zone].parse(team_digest_time).in_time_zone("UTC")
    email_log = @team.email_logs.where("slack_sent = ? AND user_id is ? AND date(sent_date) = ?", true, nil, Date.today)
    if !email_log.present? && @team.team_setting.slack_enabled
      if ((DateTime.current - 14.minutes..DateTime.current + 1.minutes).cover?team_digest_time_utc) && (@team.team_setting.days.include?(team_digest_time_utc.wday.to_s))
        slack_date = @team.question_date
        date = slack_date.strftime("%A, %d %b %Y")
        @team.email_logs.create(slack_sent: true, sent_date: DateTime.now)
        @posts = Post.digest_content(@team.id, slack_date).group_by(&:user)
        @commits = ExtActivity.commits(@team.id, slack_date).group_by(&:user)
        users = (@posts.keys + @commits.keys).uniq.compact
        @activity= {}
        users.each do |user|
          @activity[user] = [@posts[user], @commits[user]].compact.flatten
        end
        post_array = []
        text_array = []
        if @activity.present?
          @activity.each do |user, posts|
            count = posts.count {|x| x.is_a?ExtActivity}
            digest = "\n" + "*" + user.name + "*" +  "\n"
            posts.each do |post|
              if post.is_a?Post
                digest = digest + "\n" + "*Yesterday:*   " + "_" +post.today+ "_" + "\n"
                digest = digest + "\n" + "*Today:*   " + "_"+ post.tomorrow + "_" + "\n"
                digest = digest + "\n" + "*Blockers:*   " + "_" + post.blockers + "_" + "\n"
              end
            end
            if @team.team_setting.github_enabled
              digest = digest + "\n" + "*#{count}*" + "  Github Commits" + "\n"
            end
            post_array << digest
          end
          for i in 0..post_array.count
            text_array << {"color" => "#{SecureRandom.hex(3)}", "text" => "#{post_array[i]}", "mrkdwn_in" => ["text"]  }
          end
        else
          digest =  "It looks like we haven't received any updates from the team members yesterday"
        end
        team_settings = TeamSetting.find_by(team_id: @team.id)
        conn = Faraday.new(url: team_settings.slack_url)

        conn.post do |req|
          req.headers['Content-Type'] = 'application/json'
          req.body = {
            "text" => "*Digest for #{date}*\n #{digest}",
            "mrkdwn" => true,
            "attachments" =>
              text_array.each do |text|
                [text]
              end
          }.to_json
        end
      end
    end
  end
end

