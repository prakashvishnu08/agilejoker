$(document).ready(function() {
    var scntDiv = $('#p_scents');
    var i = $('#p_scents p').size() + 1;
      $('#addScnt').click(function() {
        $('<div class = "row"><p><label for="p_scnts"><div class = "col-md-4"><input type="email" id="email" name="email['+i+']" placeholder= "Email" class= "form-control" required = "true" /></div><div class = "col-md-4"><input type="text" id="name" name="name['+i+']" class= "form-control" placeholder = "Name" required = "true"/></div></label> <div class = "col-md-2"><a href="#" id="remScnt">Remove</a></div></p></div>').appendTo(scntDiv);
        i++;
        return false;
      });
      $(document.body).on('click', "#remScnt", function(e){
        if( i > 1 ) {
          $(this).parent('div').parent('div').remove();
          i--;
        }
        return false;
      });
});

$(document).ready(function(){
  $(document.body).on('click', '#role-change', function(){
    message = confirm("Are you sure?\n\n Note: Watcher gets only digest email and \nMember will get both digest and question emails.")
    var checked = $(this).data('checked');
    var team_id = $(this).data('team');
    var user_id = $(this).data('userId');

    if(message == true){
      $.ajax({
        type: "POST",
        url: "/teams/"+team_id+"/role_change",
        data: { user_id: user_id, checked: checked }
      });
    }
  });
});

$(document).ready(function(){
  var offset = - new Date().getTimezoneOffset();
  Cookies.set('browser.tzoffset', offset, {expires: 1} );
});

$(document).ready (function(){
  $("#success-alert").fadeTo(10000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
  });
});

$(document).ready (function() {
  $('.datepicker').datepicker(
    {
      dateFormat: 'dd-mm-yy',
      yearRange: "1900:2200"
    }
  );
  $('.tmw-datepicker').datepicker(
    {
      dateFormat: 'dd-mm-yy',
      appendText: "( Due on )",
      yearRange: "1900:2200",
      minDate: 1,
      setDate: 1
    }
  );
  $(".tmw-datepicker").datepicker("setDate", "1");
});

$(document).ready(function() {
  $('#user-select').multiselect({
      includeSelectAllOption: true,
      buttonWidth: '100%',
      numberDisplayed: 1,
      nonSelectedText: 'Choose User'
    });
});

function checkPasswordMatch() {
    var password = $("#txtNewPassword").val();
    var confirmPassword = $("#txtConfirmPassword").val();

    if (password != confirmPassword)
      $("#divCheckPasswordMatch").html("Passwords do not match!").css('color','red');
    else
      if ((password == "") && (confirmPassword == ""))
        $("#divCheckPasswordMatch").html("Please enter your password").css('color','red');
      else
        $("#divCheckPasswordMatch").html("Passwords match.").css('color','green');
}

$(document).ready(function () {
   $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
});

//set value for link to using dropdown
$(document).ready(function () {
  var team_id = $('#select_team').val();
  var url = "/v2/teams/" + team_id + "/tasks/prepare_tasks";
  $('#link_post_task').attr('href', url);
  $('#select_team').change(function(){
      var team_id = $(this).val();
      var url = "/v2/teams/" + team_id + "/tasks/prepare_tasks";
      $('#link_post_task').attr('href', url);
  });
});

function changeAssignee(selected, task_id, team_id) {
  $("#change" + task_id).show();
  $.ajax({
    url:'/v2/teams/' +  team_id + '/tasks/'+ task_id + '/change_assignee',
    type:'PUT',
    data:{
      assignee_id: selected.value
    }
  });
}