$(document).ajaxComplete(function(){
  set_days_multiselect(7);
});

$(document).ready(function(){
  set_days_multiselect(5);
});

function set_days_multiselect(displayCount) {
  var displayCount = displayCount;
  $('#days-select').multiselect({
    includeSelectAllOption: true,
    buttonWidth: '100%',
    numberDisplayed: displayCount,
    nonSelectedText: 'Choose Days'
  });
}

$(document).on('shown.bs.modal', function (){
  $('#button').removeAttr('href');
  $('#txtConfirmName').keyup(checkNameMatch);
});

function checkNameMatch() {
  var name = $('#txtConfirmName').data('name');
  var confirm_name = $("#txtConfirmName").val();
  var url = $('#button').data('url');

  if (name == confirm_name){
    $('#button').removeAttr("disabled");
    $('#button').attr("href",url);
  }else{
    $('#button').attr("disabled","true");
    $('#button').removeAttr('href');
  }
}

$(document).ready(function() {
  $('#github_enabled').change(function(){
    if(this.checked) {
        $("#config_repos").show();
    } else {
        $("#config_repos").hide();
    }
  });
});

$(document).ready(function() {
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    localStorage.setItem('toggleTab', $(this).attr('href'));
  });
  var toggleTab = localStorage.getItem('toggleTab');
  if (toggleTab) {
    $('[href="' + toggleTab + '"]').tab('show');
  }
});

$(document).ready(function() {
  $('.btn-group > .btn').click(function(){
    $(this).addClass('active').siblings().removeClass('active');
  });
});

$(document).ready(function() {
  $('#default-task').click();
});
