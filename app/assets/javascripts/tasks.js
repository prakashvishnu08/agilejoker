$(document).ready(function() {
  $('#default-task').on('click', function(){
    $('#team-tasks').html("");
    $('#my-tasks').show();
    $(this).click(function () {
      $('#my-tasks').html("");
     });
  });
  $('#team-task').on('click', function(){
    $('#team-tasks').show();
    $('#my-tasks').html("");
    $(this).click(function () {
      $('#team-tasks').html("");
     });
  });
});

$(function(){
  var isLoading = false;
  if ($('#infinite-scrolling').size() > -1) {
    $(window).scroll(function() {
      var more_posts_url = $('.pagination a.next_page').attr('href');
      if (!isLoading && more_posts_url && $(window).scrollTop() > $(document).height() - $(window).height() - 50) {
        isLoading = true;
        $.getScript(more_posts_url).done(function (data,textStatus,jqxhr) {
          isLoading = false;
        }).fail(function() {
          isLoading = false;
        });
      }
    });
  }
});

$('#hash').keyup(function() {
  if($(this).val().length != 0){
    $('#from_date').attr('disabled', false);
    $('#to_date').attr('disabled', false);
  }else{
    $('#from_date').attr('disabled', true);
    $('#to_date').attr('disabled', true);
  }
});

$('#hash_submit').click(function () {
  if ($('#from_date').val() != 0) {
    $("#to_date").attr('required', true);
  }else{
    $("#to_date").attr('required', false);
  }
  if ($('#to_date').val() != 0) {
    $("#from_date").attr('required', true);
  } else{
    $("#from_date").attr('required', false);
  }
});

$('#from_date').datepicker({
   dateFormat: 'dd/mm/yy',
  onSelect: function() {
    var dt = $('#from_date').datepicker('getDate');
    dt.setDate(dt.getDate());
    $('#to_date').datepicker("option", "minDate", dt);
  }
});

$('#to_date').datepicker({
    dateFormat: 'dd/mm/yy'
});
