$(document).ready(function() {
  $('#user-list').multiselect({
      includeSelectAllOption: true,
      buttonWidth: '100%',
      numberDisplayed: 1,
      nonSelectedText: 'Choose User'
    });
  $("#team-change").change(function() {
    $.ajax({
      url: "/v2/tasks/find_users",
      type: "GET",
      data: {team_id: $('#team-change option:selected').val()},
    })
  });
});
