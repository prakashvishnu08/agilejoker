module TasksHelper
  def render_with_hashtags(text, team_id)
    text.gsub(/(?:#(\w+))/) {hashtag_link($1, team_id)}.html_safe
  end
  
  def hashtag_link(hash, team_id)
    link_to "##{hash}", tasks_list_v2_team_tags_path(hash: hash, team_id: team_id)
  end

  def status(task)
    status = { }
    if task.completed_on.present?
       status[:text] = "Completed"
       status[:class] = "label-success"
    elsif task.blocker?
       status[:text] = "Blocker"
       status[:class] = "label-danger"
    else
      status[:text] = "Pending"
      status[:class] = "label-primary"
    end
    status
  end
end
