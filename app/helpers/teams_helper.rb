module TeamsHelper
  def question_time
    ['2:00 PM', '3.00 PM', '4:00 PM', '5:00 PM', '6:00 PM', '7:00 PM', '8:00 PM', '9:00 PM', '10:00 PM']
  end

  def digest_time
    ['3:00 AM', '4:00 AM', '5:00 AM', '6.00 AM', '7:00 AM', '8:00 AM', '9:00 AM', '10:00 AM', '11:00 AM']
  end

  def team_addition_date(team, user)
    format_date(team.allotment(user).try(:joined_at))
  end

  def team_addition_info(team, user)
    if team.approved?(user)
      "Joined on #{team_addition_date(team, user)}"
    end
  end
end
