module ApplicationHelper

  def format_date(date)
    date.strftime("%d/%m/%Y") if date
  end

  def title(page_title)
    content_for :title, page_title.to_s
  end

  def is_active?(controller, action)
    return 'active' if request.parameters['controller'] == controller && request.parameters['action'] == action
  end

end
