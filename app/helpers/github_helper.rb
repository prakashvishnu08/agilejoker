module GithubHelper

  def github_authorize_url
    "https://github.com/login/oauth/authorize?&client_id=#{ENV['GITHUB_ID']}&scope=admin:repo_hook"
  end
end
