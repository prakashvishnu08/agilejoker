module PostsHelper

  def editable(received_date, team)
    if team.email_logs.where("date(sent_date) = ?", received_date.to_date).present? &&
      team.email_logs.where("date(sent_date) = ? and digest_sent = ?", received_date.to_date, true).count == 0
      true
    elsif
      team.email_logs.count  == 0
      true
    else
      false
    end
  end
end
