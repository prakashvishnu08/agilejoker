class Task < ActiveRecord::Base
  belongs_to :team
  belongs_to :owner, class_name: "User", foreign_key: "created_by"
  belongs_to :assignee, class_name: 'User', foreign_key: 'assigned_to'
  has_many :taggings
  has_many :tags, through: :taggings
  validates :title, :team, :owner, presence: true

  after_create :create_tags
  STATUS = ["Blocker", "Completed", "Pending"]

  def create_tags
    tags = self.title.scan(/#([\w\.-]+)/).flatten
    ActiveRecord::Base.transaction do
      tags.each do |name|
        new_tag = Tag.where(name: name, team_id: self.team_id).first_or_create do |tag|
          tag.team_id = self.team_id
          tag.created_by = self.created_by
        end
        self.tags << new_tag
      end
    end
  end

  def completed?
    self.completed_on.present?
  end

  def self.status(param)
    if param == "Completed"
      where.not(completed_on: nil)
    elsif param == "Blocker"
      where(blocker: true)
    elsif param == "Pending"
      where(blocker: false, completed_on: nil)
    end
  end

  def self.ransackable_scopes(auth_object = nil)
    %i(status)
  end

end
