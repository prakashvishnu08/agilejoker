class Tag < ActiveRecord::Base
  belongs_to :team
  belongs_to :owner, class_name: "User", foreign_key: "created_by"
  has_many :taggings
  has_many :tasks, through: :taggings

  validates :name, uniqueness: {scope: :team_id}

  enum tag_type: [ :project, :goal, :general ]
end
