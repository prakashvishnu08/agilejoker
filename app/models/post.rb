class Post < ActiveRecord::Base
  require 'csv'
  acts_as_paranoid
  belongs_to :user
  belongs_to :team

  self.per_page = 10
  DAYS_LIMIT = 15

  def date
    received_date.to_date
  end

  def self.digest_content(team_id, date_utc)
    includes(:user).where("team_id = ? AND date(received_date) = ?",
    team_id, date_utc.to_date ).order("received_date DESC")
  end

  def self.to_csv list
    CSV.generate do |csv|
      csv << ["Team", "User Name", "Completed", "Received Date"]
      list.each do |post|
      	team_user = [ post.team_name, post.user_name ]
      	post_details = post.attributes.values_at("today", "received_date")
      	required_data = team_user + post_details
        csv << required_data
      end
    end
  end

  def team_name
  	self.team.name
  end

  def user_name
  	self.user.name ? self.user.name : self.user.email
  end
end
