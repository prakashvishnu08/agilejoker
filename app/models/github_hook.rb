class GithubHook < ActiveRecord::Base
  belongs_to :team
  before_create :set_token

  def self.create_webhook(client, team_id, repo_fullname, token)
    client.create_hook(repo_fullname, "web",
      {
        url: "#{ENV['APP_DOMAIN']}/v2/github_hooks/teams/#{team_id}/payload",
        secret: token,
        content_type: 'json',
        events: ['push']
      },
      { :active => true })
  end

private

  def set_token
    return if token.present?
    self.token = generate_token("agilejoker", self.team.uid)
  end

  def generate_token(key, data)
    digest = OpenSSL::Digest.new('sha1')
    OpenSSL::HMAC.hexdigest(digest, key, data)
  end
end
