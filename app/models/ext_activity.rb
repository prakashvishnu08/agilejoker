class ExtActivity < ActiveRecord::Base
  belongs_to :user
  belongs_to :team

  def date
    event_time.to_date
  end

  def self.commits(team_id, date_utc)
    where("team_id = ? AND date(event_time) = ?",
    team_id, date_utc.to_date)
  end

end
