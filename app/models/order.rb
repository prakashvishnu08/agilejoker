class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :subscription
  belongs_to :plan

  enum status: [ :initiated, :failed, :pending, :success ]

  def purchase
    response = ::GATEWAY.purchase(order_amount_cents, express_purchase_options)
    self.update_attribute(:purchased_at, Time.now) if response.success?
    response.success?
  end

  private

  def express_purchase_options
    {
      :ip => ip,
      :token => paypal_token,
      :payer_id => paypal_payer_id
    }
  end

  def order_amount_cents
    (amount * 100).round
  end
end
