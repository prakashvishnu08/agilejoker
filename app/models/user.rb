class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :posts, dependent: :destroy
  has_many :allotments, dependent: :destroy
  has_many :email_logs, dependent: :destroy
  has_many :teams, through: :allotments
  has_many :subscriptions

  after_create :set_free_plan
  validates :photo, file_size: { less_than_or_equal_to: 2.megabyte }
  mount_uploader :photo, UserAvatarUploader

  scope :spanning, ->(from_time, to_time) { where("users.created_at >= ? AND users.created_at < ?", from_time, to_time) }

  def set_free_plan
    self.subscriptions.create(active: true, plan_code: 111, start_date: DateTime.now, end_date: DateTime.now + 365.days)
  end

  scope :confirmed, -> { where(status: true) }

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  def member?(team)
    team_allotment(team).first.role == 'member'
  end

  def observer?(team)
    team_allotment(team).first.role == 'observer'
  end

  def team_allotment(team)
    team.allotments.where(user_id: self.id)
  end

  def user_detail
    if self.name.present?
      self.name
    else
      self.email
    end
  end

  def in_team?
    team = Allotment.where(user_id: self.id).first
    if team.present?
      false
    else
      true
    end
  end

  def team_admin(team)
    self.id == team.created_by
  end

  def approve_team(team)
    team.allotments.where(user_id: self.id).first.update_columns(accepted: true, joined_at: DateTime.now)
    admin = User.find_by(id: team.created_by)
    if self != admin
      AdminMailer.new_join(self, admin, team).deliver_later
    end
    V2::UserMailer.team_confirm_email(self, team).deliver_later
  end

  def send_invite(team, current_user)
    V2::UserMailer.team_invite(self, team, current_user).deliver_later
  end

  def resend_invite(team, current_user)
    if self.confirmed?
      V2::UserMailer.team_invite(self, team, current_user).deliver_later
    else
      self.resend_confirmation_instructions
    end
  end

  def attempt_set_password(params)
    p = {}
    p[:password] = params[:password]
    p[:password_confirmation] = params[:password_confirmation]
    update_attributes(p)
  end

  # new function to return whether a password has been set
  def has_no_password?
    self.encrypted_password.blank?
  end

  # Devise::Models:unless_confirmed` method doesn't exist in Devise 2.0.0 anymore.
  # Instead you should use `pending_any_confirmation`.
  def only_if_unconfirmed
    pending_any_confirmation {yield}
  end

  def password_required?
  # Password is required if it is being set, but not for new records
    if !persisted?
      false
    else
      !password.nil? || !password_confirmation.nil?
    end
  end

  def plan
    code = subscriptions.where(active: true).last.plan_code
    Plan.where(code: code).first
  end

  #for avoiding confirmation email for time being
  # protected
  #   def confirmation_required?
  #     false
  #   end

end
