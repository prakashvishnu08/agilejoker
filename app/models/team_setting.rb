class TeamSetting < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :team
  serialize :days, Array
  mount_uploader :photo, AvatarUploader

  validates :question_email_at, :digest_email_at, presence: true
  validates :photo, file_size: { less_than_or_equal_to: 2.megabyte }

end
