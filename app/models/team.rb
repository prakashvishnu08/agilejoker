class Team < ActiveRecord::Base
  acts_as_paranoid
  has_many :email_logs, dependent: :destroy
  has_one  :team_setting, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :allotments, dependent: :destroy
  has_many :tags, dependent: :destroy
  has_many :tasks, dependent: :destroy
  belongs_to :owner, class_name: "User", foreign_key: "created_by"
  has_many :ext_activities, dependent: :destroy
  has_many :github_hooks, dependent: :destroy
  has_many :users, through: :allotments do
    def accepted
      where("allotments.accepted = ?", true)
    end
  end

  accepts_nested_attributes_for :team_setting
  delegate(
    :question_email_at,
    :digest_email_at,
    :timezone,
    :photo,
    :days,

    to: :team_setting,
    allow_nil: true
  )

  validates :name, presence: true
  before_create :set_uid

  scope :spanning, ->(from_time, to_time) { where("teams.created_at >= ? AND teams.created_at < ?", from_time, to_time) }

  def set_uid
    team_name = self.name.parameterize("_")
    self.uid = team_name + "_" + SecureRandom.uuid[0..3]
  end

  def email
    "#{uid}@#{ENV['EMAIL_DOMAIN']}"
  end

  def approved?(user)
    allotment(user).accepted
  end

  def self.default_team(user)
    @user = user
    @team = Team.new(default_params)
    @team.save!
    @team.users << user
    user.approve_team(@team)
    @team
  end

  def member_count
    users.accepted.size
  end

  def allotment(user)
    allotments.where(user_id: user.id).first
  end

  def members
    allotments.where(role: ['owner', 'member']).map(&:user)
  end

  def watchers
    allotments.where(role: 'observer').map(&:user)
  end

  def digest_day(user)
    log = self.email_logs.where(user_id: user.id, digest_sent: false).order("sent_date DESC").first
    if log.present?
      digest_day = log.sent_date
    end
  end

  def question_date
    days = self.team_setting.days.sort
    if days.present?
      today = Date.today
      last_day = (days.select{ |day| day.to_i < today.wday }.max || days.last).to_i
      if last_day < today.wday
        diff = today.wday - last_day
      else
        diff = 7-(last_day - today.wday)
      end
      date = today - diff.days
    end
  end

  private

  def self.default_params
    default_values = {name: default_team_name, created_by: @user.id}
    default_values[:team_setting_attributes] = { question_email_at: "6:00 PM", digest_email_at: "10:00 AM", timezone: "Chennai", days: [ "1", "2", "3", "4", "5"]}
    default_values
  end

  def self.default_team_name
    if @user.name.present?
      @user.name.split(" ")[0] + "'s Team"
    else
      "My Team"
    end
  end

end
