class Allotment < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :user
  belongs_to :team

  scope :spanning, ->(from_time, to_time) { where("allotments.created_at >= ? AND allotments.created_at < ?", from_time, to_time) }
end
