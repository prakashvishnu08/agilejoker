class EmailLog < ActiveRecord::Base
  acts_as_paranoid
  before_create :set_auth_token
  belongs_to :team
  belongs_to :user

  private

  def set_auth_token
    return if auth_token.present?
    self.auth_token = generate_auth_token
  end

  def generate_auth_token
    SecureRandom.uuid.gsub(/\-/,'')
  end
end
