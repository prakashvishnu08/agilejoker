class RegistrationsController < Devise::RegistrationsController
  layout 'application', only: [:edit, :update]
   prepend_before_action :check_captcha, only: [:create]
  after_action :save_user_timezone, only: [:create]


  private

  def check_captcha
    unless verify_recaptcha
      self.resource = resource_class.new sign_up_params
      respond_with_navigational(resource) { render :new }
    end
  end

  def save_user_timezone
  	return unless resource.persisted?
    if js_timezone_offset.present?
      offset_seconds = js_timezone_offset.to_i * 60
      timezone = ActiveSupport::TimeZone[offset_seconds]
      resource.update(timezone: timezone.name)
    end
  end

  def js_timezone_offset
    cookies['browser.tzoffset']
  end

  def sign_up_params
    params.require(:user).permit(:name, :timezone, :email, :password, :password_confirmation)
  end

  def after_inactive_sign_up_path_for(resource)
    new_user_session_path
  end

end
