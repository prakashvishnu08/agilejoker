class OrdersController < ApplicationController
  before_action :authenticate_user!

  def new
    @plan = Plan.find(params[:plan_id])
    @order = Order.new
  end

  def create
    @order = Order.new(order_params)
    if @order.save
      @order.update_columns(status: Order.statuses[:pending])
      checkout_paypal
    else
      render 'new'
    end
  end

  def success
    @order = Order.find(params[:id])
    @order.update_columns(paypal_payer_id: params[:PayerID])
    if @order.purchase
      @subscription = current_user.subscriptions.create(
        plan_code: @order.plan.code,
        start_date: DateTime.now,
        end_date: DateTime.now + 30.days,
        active: true
      )
      @order.subscription_id = @subscription.id
      @order.status = Order.statuses[:success]
      @order.save
    else
      redirect_to error_order_url(@order)
    end
     flash[:notice] = "Thank you for the subscription.Contact support@agilejoker.com if you face any issues"
     redirect_to root_path
  end

  def error
    @order = Order.find(params[:id])
    @order.status = Order.statuses[:failed]
    @order.save
    @s = GATEWAY.details_for(@order.paypal_token) # To get details of payment
    flash[:alert] = "Something went wrong! Try again making payment. Contact support@agilejoker.com if you face any further issues"
    redirect_to root_path
    # @s.params['message'] gives you error
  end


  private

  def checkout_paypal
    paypal_response = ::GATEWAY.setup_purchase(
      (@order.amount * 100).round, # paypal amount is in cents
      ip: request.remote_ip,
      return_url: success_order_url(@order), # return here if payment success
      cancel_return_url: error_order_url(@order), # return here if payment failed
      currency: "USD",
      allow_guest_checkout: true,
      items: [{name: "AgileJoker", description: "Monthly Subscription", quantity: "1", amount: (@order.amount * 100).round}]
    )
    @order.paypal_token = paypal_response.token # save paypal token to db
    @order.save
    redirect_to ::GATEWAY.redirect_url_for(paypal_response.token) and return  # redirect to paypal for payment
  end

  def order_params
    params.require(:order).permit(:user_id, :plan_id, :amount, :status).merge(user_id: current_user.id, status: Order.statuses[:initiated], ip: request.remote_ip)
  end
end
