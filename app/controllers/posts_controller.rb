class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_team, only: [:new]

  def index
  	params[:q] ||= {}
    if params[:q][:received_date_lteq].present?
      params[:q][:received_date_lteq] = params[:q][:received_date_lteq].to_date.end_of_day
    end
  	@teams = Team.where(created_by: current_user.id)
  	@users = @teams.map{|team| team.users.accepted}.flatten.uniq
  	@q = Post.where(team_id: @teams.map { |e| e.id  }).ransack(params[:q])
  	@q.sorts = 'received_date desc' if @q.sorts.empty?
  	@csv_posts = @q.result.includes(:team, :user)
    @posts = @q.result.includes(:team, :user).paginate(:page => params[:page])
    authorize @posts
  end

  def search
    index
    respond_to do |format|
      format.html {render :index}
      format.csv { send_data Post.to_csv(@csv_posts), :filename => "Report_agile_" + "#{Time.now}.csv", :type => "text/csv" }
    end
  end

  def new
    @post = Post.new
    @posts = Post.where(team_id: @team, user_id: current_user.id).paginate(:page => params[:page],per_page: 10)
    @posts = @posts.where("received_date >= '#{(Post::DAYS_LIMIT).days.ago}'").paginate(:page => params[:page],per_page: 10) if current_user.plan.data_retention?
    @user_posts = @posts.order("received_date DESC").group_by(&:date)
    respond_to do |format|
      format.js
    end
  end

  def create
    @post = Post.create(post_params)
    @team = @post.team
    if @post.save
      flash[:notice] = "Updates submitted successfully"
      redirect_to @team
    else
      render 'new'
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    @team = @post.team
    if @post.update(post_params)
      flash[:notice] = "Updates edited successfully"
      redirect_to @team
    else
      render 'edit'
    end
  end

  private

  def post_params
    params.require(:post).permit(:today, :tomorrow, :blockers, :user_id, :team_id, :received_date)
  end

  def set_team
    @team = Team.find(params[:team_id])
  end

end
