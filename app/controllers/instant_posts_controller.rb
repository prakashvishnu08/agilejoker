class InstantPostsController < ApplicationController
  before_action :authenticate_token

  def instant_updates
    @token =  @email_log.auth_token
      @user = @email_log.user
      @team = @email_log.team
      @post = Post.new
  end

  def save_updates
    @post = Post.create(post_params)
    if @post.save
      @email_log = EmailLog.where(user_id: params[:post][:user_id], team_id: params[:post][:team_id]).last
      remove_token = @email_log.update(auth_token: nil)
      flash[:notice] = "Updates submitted successfully"
      render 'thank_you'
    end
  end

  private

  def authenticate_token
    token = params[:token] || params[:post][:token]
    @email_log = EmailLog.find_by(auth_token: token)
    if !@email_log.present?
      render 'invalid'
    end
  end

  def post_params
    params.require(:post).permit(:today, :tomorrow, :blockers, :user_id, :team_id, :received_date)
  end
end

