class GithubHooksController < ApplicationController
  before_action :authenticate_token, only: :payload
  before_action :authenticate_user!, except: :payload
  skip_before_action :verify_authenticity_token, only: [:payload]


  def new
    client = Octokit::Client.new access_token: team_settings.github_token
    @repos = client.repos
  end

  def create
    repos = params[:repos]
    CreateGithubHookJob.perform_later(team_id, repos, team_settings)
    redirect_to root_path, notice: "Github Integrated Successfully"
  end

  def edit
    @team = Team.find(params[:id])
    team_settings = TeamSetting.find_by(team_id: params[:id])
    client = Octokit::Client.new access_token: team_settings.github_token
    @repos = client.repos
  end

  def update
    team = Team.find(params[:id])
    repos = params[:repos]
    GithubUpdateJob.perform_later(team, repos, team_settings)
    redirect_to root_path, notice: "Github Repos Configured!"
  end

  def payload
    GithubPayloadJob.perform_later(params)
    render :nothing => true
  end

  def authenticate_token
    hook = GithubHook.where(repo_id: params[:repository][:id], team_id: params[:team_id]).last
    if !token_valid?(hook)
      render :nothing => true, status: :unauthorized
    end
  end

  private

  def team_id
    session[:team_id]
  end

  def team_settings
    TeamSetting.find_by(team_id: team_id)
  end

  def token_valid?(hook)
    digest = OpenSSL::Digest.new('sha1')
    request.headers['X-Hub-Signature']=="sha1=#{OpenSSL::HMAC.hexdigest(digest, hook.token, request.raw_post)}"
  end

end
