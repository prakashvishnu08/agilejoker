class GithubController < ApplicationController
  before_action :authenticate_user!, except: [:payload]
  skip_before_action :verify_authenticity_token, only: [:payload]

  def callback
    response = Octokit.exchange_code_for_token(params[:code], ENV['GITHUB_ID'], ENV['GITHUB_SECRET'])
    token = response[:access_token]
    if team_settings.update(github_token: token, github_enabled: true)
      redirect_to new_github_hook_path
    else
      render text: "Oops! There was a problem."
    end
  end

  private

  def team_id
    session[:team_id]
  end

  def team_settings
    TeamSetting.find_by(team_id: team_id)
  end
end
