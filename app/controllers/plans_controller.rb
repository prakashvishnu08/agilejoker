class PlansController < ApplicationController
  before_action :authenticate_user!

  def upgrade
    SuperAdminMailer.upgrade_interest(current_user, params[:plan_id]).deliver_now
  end

  def index
  end
end
