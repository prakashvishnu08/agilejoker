module V2
  class TagsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_team


    def tasks_list
      if params.values_at(:from_date, :to_date).all?(&:present?)
        tag_tasks = Tag.find_by(name: params[:hash], team_id: @team.id).try(:tasks)
        tasks = tag_tasks.where("DATE(tasks.created_at) BETWEEN ? AND ?", params[:from_date].to_date,
                params[:to_date].to_date) if tag_tasks.present?
      else
        tasks = Tag.where(name: params[:hash], team_id: @team.id).first.try(:tasks)
      end
      @tasks = tasks.paginate(:page => params[:page], :per_page => 10) if tasks.present?
    end

    private

    def set_team
      @team = Team.find(params[:team_id])
    end

  end
end
