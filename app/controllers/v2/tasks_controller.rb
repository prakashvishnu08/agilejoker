module V2
  class TasksController < ApplicationController
    before_action :authenticate_user!
    before_action :set_team, only: [ :new, :hashtasks, :show, :add_my_task,
                                     :prepare_tasks, :complete, :blocker,
                                     :reset, :user_task, :my_task, :team_task
                                   ]
    before_action :find_tasks, only: [:prepare_tasks, :complete, :blocker, :reset]
    def new
      @task = Task.new
    end

    def create
      @task =Task.new(task_params)
      if @task.save
        flash[:notice] = 'Task added successfully'
        redirect_to v2_team_task_path(@task, team_id: @task.team_id)
      else
        render :new
      end
    end

    def hashtasks
      @tasks = Tag.where(name: params[:hash], team_id: @team.id).first.tasks
    end

    def show
      @task  = Task.find(params[:id])
    end

    def prepare_tasks
      @task = Task.new
    end

    def add_my_task
      @task = Task.new(task_params)
      @task.assign_attributes(due_on: set_due_date(task_params), assigned_to: current_user.id)
      if @task.save
        redirect_to prepare_tasks_v2_team_tasks_path
      else
        flash[:alert] = 'Something went wrong.Try again'
        redirect_to prepare_tasks_v2_team_tasks_path
      end
    end

    def complete
      @task = Task.find(params[:id])
      @task.assign_attributes(completed_on: DateTime.now, blocker: false)
      @task.save
      respond_to do |format|
        format.js
      end
    end

    def reset
      @task = Task.find(params[:id])
      @task.completed_on = nil
      @task.save
      respond_to do |format|
        format.js
      end
    end

    def blocker
      @task = Task.find(params[:id])
      @task.blocker = true
      @task.save
      respond_to do |format|
        format.js
      end
    end

    def change_assignee
      @task = Task.find(params[:id])
      @task.assigned_to = params[:assignee_id]
      @task.save
      respond_to do |format|
        format.js
      end
    end

    def user_task
    end

    def my_task
      @users = Team.find(params[:team_id]).users
      @tasks = Task.where(assigned_to: current_user.id, team_id: @team.id).order('created_at DESC').paginate(page: params[:page], per_page: 20)
      respond_to do |format|
        format.js
      end
    end

    def team_task
      @users = Team.find(params[:team_id]).users
      @tasks = Task.includes(:assignee).where(team_id: @team.id).order('created_at DESC').paginate(:page => params[:page], per_page: 20)
      respond_to do |format|
        format.js
      end
    end

    def find_users
      @users = Team.find(params[:team_id]).users.accepted
      respond_to do |format|
        format.js
      end
    end

    def report
      @teams = Team.where(created_by: current_user.id)
      @users = @teams.map{|team| team.users.accepted}.flatten.uniq
      @q = Task.where(team_id: @teams.map { |e| e.id  }).ransack(params[:q])
      @tasks = @q.result.includes(:assignee, :team).paginate(:page => params[:page], per_page: 20)
    end

    private

    def task_params
      params.require(:task).permit(:title, :due_on, :completed_on, :blocker, :team_id, :created_by).merge(created_by: current_user.id)
    end

    def set_team
      @team = Team.find(params[:team_id])
    end

    def set_due_date(task_params)
      if task_params.has_key?(:due_on)
        task_params[:due_on]
      else
        DateTime.now
      end
    end

    def find_tasks
      @task = Task.new
      @tasks_today = Task.where(due_on: Date.today.beginning_of_day..Date.today.end_of_day, assigned_to: current_user.id, team_id: params[:team_id]).order('created_at DESC')
      @tasks_tmw = Task.where(due_on: (Date.today + 1.days).beginning_of_day..(Date.today + 1.days).end_of_day, assigned_to:current_user.id, team_id: params[:team_id]).order('due_on DESC, created_at DESC')
    end

  end
end
