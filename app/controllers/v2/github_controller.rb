module V2
  class GithubController < ApplicationController
    before_action :authenticate_user!

    def callback
      response = Octokit.exchange_code_for_token(params[:code], ENV['GITHUB_ID'], ENV['GITHUB_SECRET'])
      token = response[:access_token]
      if token.present?
        if team_settings.update(github_token: token, github_enabled: true)
          redirect_to new_v2_github_hook_path
        end
      else
        render text: "Oops! There was a problem."
      end
    end

    private

    def team_id
      session[:team_id]
    end

    def team_settings
      TeamSetting.find_by(team_id: team_id)
    end
  end
end
