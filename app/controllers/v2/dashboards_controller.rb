module V2
  class DashboardsController < ApplicationController
    before_action :authenticate_user!

    def index
      @posts = current_user.posts.order("created_at DESC")
      @teams = current_user.teams
    end
    # Add Contextual messages. This will be a place for contextual messages. If the user has logged in for the first time, a message is set accordingly. If the user has not set a status for today, while other team members have put in their statuses; another message. Drive some action from this message.

    def start
    end
  end
end
