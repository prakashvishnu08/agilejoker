module V2
  class TeamsController < ApplicationController
    before_action :authenticate_user!

    def new
      @team  = Team.new
      authorize @team
      @team.build_team_setting
    end

    def index
      @teams = current_user.teams
    end

    def create
      @team = Team.new(team_params)
      params[:team][:team_setting_attributes][:days] ||= []
      if @team.save
        @team.users << current_user
        current_user.approve_team(@team)
        flash[:notice] = "Team created successfully"
        redirect_to prepare_tasks_v2_team_tasks_path(@team)
      else
        render :new
      end
    end

    def show
      @team = Team.includes(:users).find(params[:id])
      authorize @team
      redirect_to prepare_tasks_v2_team_tasks_path(@team)
    end

    def edit
      @team = Team.find(params[:id])
      session[:team_id] = @team.id
    end

    def update
      @team = Team.find(params[:id])
      params[:team][:team_setting_attributes][:days] ||= []
      if @team.update(edit_params)
        flash[:notice] = "Team Settings Updated"
        redirect_to edit_v2_team_path(@team)
      else
        render :edit
      end
    end

    def activity
      @team = Team.find(params[:id])
      if current_user.plan.data_retention?
        @team_posts = Post.joins(:user).includes(:user)
          .where(team_id: params[:id])
          .where("received_date >= '#{(Post::DAYS_LIMIT).days.ago}'")
          .paginate(:page => params[:page],per_page: 10)
      else
        @team_posts = Post.joins(:user).includes(:user)
          .where(team_id: params[:id])
          .paginate(:page => params[:page],per_page: 10)
      end
      @posts = @team_posts.order("received_date DESC, users.name ASC").group_by(&:date)
    end

    def members
      @team = Team.find(params[:id])
    end

    def approve_request
      team = Team.find(params[:id])
      current_user.approve_team(team)
      redirect_to v2_team_path(team)
    end

    def add_users
      authorize team
      email_list = params[:email].delete_if { |k, v| v.empty? }
      ob_list = params[:observer].delete_if { |k, v| v.empty? } if params[:observer]

      if email_list.present?
        email_list.each do |key,email|
          user = User.where(email: email).first
          if !user.present?
            user = User.new(email: email, status: false)
            user.save!(validate: false)
          end
          if !team.users.include?user
            role = 'observer' if ob_list && ob_list[key] == "1"
            user.allotments.create!(team_id: team.id, role: role || 'member')
            user.send_invite(team, current_user)
            flash[:notice] = "Team members added successfully"
          else
            flash[:notice] = "User is already in the team."
          end
        end
      end
      redirect_to members_v2_team_path
    end

    def role_change
      checked = params[:checked]
      allotment = team.allotment(user)
      if checked == "false"
        role_observer = allotment.update(role: "member")
      else
        role_member = allotment.update(role: "observer")
      end
      redirect_to members_v2_team_path(team)
    end

    def resend_invitation
      authorize team, :update?
      if (!team.approved?(user)) && (team.users.include?(user))
        user.resend_invite(team, current_user)
        redirect_to members_v2_team_path(team), notice: 'Invitation sent'
      else
        redirect_to members_v2_team_path(team), notice: 'User is already in the team.'
      end
    end

    def disable
      team.destroy
      redirect_to root_path, notice: 'Team Deleted Successfully'
    end

    def remove_user
      authorize team, :update?
      authorize user
      remove_from_team = team.allotment(user).destroy
      redirect_to members_v2_team_path(team)
    end

    private

    def team
      Team.find(params[:id])
    end

    def user
      User.find(params[:user_id])
    end

    def team_params
      params.require(:team).permit(:name, :uid, team_setting_attributes: [:question_email_at, :digest_email_at, :timezone, :photo, days: []]).merge(created_by: current_user.id)
    end

    def edit_params
      params.require(:team).permit(:name, :uid, team_setting_attributes: [:id, :question_email_at, :digest_email_at, :timezone, :photo, :slack_enabled, :github_enabled, days: []])
    end
  end
end
