module V2
  class SlackController < ApplicationController
    before_action :authenticate_user!
    require "slack"

    def callback
      client = Slack::Client.new
      response = client.oauth_access(
        client_id: ENV['SLACK_ID'],
        client_secret: ENV['SLACK_SECRET'],
        code: params[:code],
        redirect_uri: ENV['SLACK_REDIRECT_URI']
      )
      team_settings = TeamSetting.find_by(team_id: session[:team_id])
      if team_settings.update_attributes(
        slack_token: response['access_token'],
        slack_url: response['incoming_webhook']['url'],
        slack_enabled: true
      )
        redirect_to root_path
        flash[:notice] = "Slack successfully integrated"
      else
        render text: "Oops! There was a problem."
      end
    end
  end
end
