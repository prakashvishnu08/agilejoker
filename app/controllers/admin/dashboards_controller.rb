module Admin
  class DashboardsController < ApplicationController
    before_action :authenticate_user!

    def today
      from_date = Date.current.beginning_of_day
      to_date = 1.day.from_now.end_of_day

      @metrics = metrics(from_date, to_date)
      set_allotments(from_date, to_date)
      authorize :super_admin, :index?
    end

    def full
      from_date = 90.days.ago.beginning_of_day
      to_date = 1.day.from_now.end_of_day

      @metrics = metrics(from_date, to_date)
      set_allotments(from_date, to_date)
      authorize :super_admin, :index?
    end

    private

    def metrics(from_date, to_date)
      signups = User.spanning(from_date, to_date)
      teams = Team.spanning(from_date, to_date)
      pending_users = User.spanning(from_date, to_date).where(status: false)
      subscriptions = User.spanning(from_date, to_date).joins(:subscriptions).group("subscriptions.plan_code").count
      plans = Hash[*Plan.pluck(:code, :name).flatten]

      {
        signups: signups.length,
        pending_users: pending_users.length,
        teams: teams.length,
        subscriptions: subscriptions,
        plans: plans
      }
    end

    def set_allotments(from_date, to_date)
      @q = Allotment
            .spanning(from_date, to_date)
            .includes(:team, :user)
            .order(:user_id, :team_id)
            .ransack(params[:q])

      @q.sorts = 'created_at desc' if @q.sorts.empty?
      @allotments = @q.result.includes(:team, :user).paginate(:page => params[:page])
    end
  end
end
