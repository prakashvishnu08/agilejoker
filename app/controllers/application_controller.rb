class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :configure_permitted_parameters, if: :devise_controller?
  around_action :set_time_zone

  protect_from_forgery with: :exception

  def configure_permitted_parameters
    update_attrs = [:password, :password_confirmation, :current_password]
    devise_parameter_sanitizer.permit :account_update, keys: update_attrs
  end

  def set_time_zone
    if js_timezone_offset.present?
      offset_seconds = js_timezone_offset.to_i * 60
      timezone = ActiveSupport::TimeZone[offset_seconds]
      Time.use_zone(timezone) { yield }
    else
      yield
    end
  end

  def js_timezone_offset
    cookies['browser.tzoffset']
  end

  def after_sign_in_path_for(user)
    if user.sign_in_count == 1
      start_path
    else
      root_path
    end
  end

  private

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore

    flash[:alert] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default
    redirect_to(request.referrer || root_path)
  end
end
