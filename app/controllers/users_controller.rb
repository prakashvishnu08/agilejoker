class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update]

  def show
  end

  def edit
    @user
  end

  def update
    if @user.update(edit_params)
      flash[:notice] = "Profile Successfully Updated"
      redirect_to user_path(@user)
    else
      render 'edit'
    end
  end

  private

  def set_user
    @user = current_user
  end

  def edit_params
    params.require(:user).permit(:id, :name, :timezone, :photo, :github_username)
  end
end
