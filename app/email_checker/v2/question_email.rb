module V2
  class QuestionEmail

    attr_reader :team

    def initialize(team)
      @team = team
    end

    def time_for_question
      @team.users.confirmed.accepted.each do |user|
        team_question_time = @team.team_setting.question_email_at
        time_zone = user.timezone ? user.timezone : @team.team_setting.timezone
        user_question_time_utc = ActiveSupport::TimeZone[time_zone].parse(team_question_time).in_time_zone("UTC")
        if ((DateTime.current - 14.minutes..DateTime.current + 1.minutes).cover?user_question_time_utc) && (@team.team_setting.days.include?(user_question_time_utc.wday.to_s))
          email_log = @team.email_logs.where(user_id: user.id).where("date(sent_date) = ?" ,user_question_time_utc.to_date)
          if email_log.present?
          elsif user.observer?(@team) && !email_log.present?
            @team.email_logs.create(user_id: user.id, sent_date: user_question_time_utc, question_sent: false, digest_sent: false)
          else
            log = @team.email_logs.create(user_id: user.id, sent_date: user_question_time_utc, question_sent: true, digest_sent: false)
            V2::UserMailer.sent_question_email(user, @team).deliver_later
            puts "Question mail send to #{user.email}"
          end
        end
      end
    end
  end
end
