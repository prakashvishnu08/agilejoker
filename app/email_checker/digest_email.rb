class DigestEmail
  attr_reader :team

  def initialize(team)
    @team = team
  end

  def time_for_digest
    @team.users.confirmed.accepted.each do |user|
      team_digest_time = @team.team_setting.digest_email_at
      time_zone = user.timezone ? user.timezone : @team.team_setting.timezone
      user_digest_time_utc = ActiveSupport::TimeZone[time_zone].parse(team_digest_time).in_time_zone("UTC")
      digest_day = @team.digest_day(user)
      date_string = digest_day.strftime("%A, %d %b %Y") if digest_day.present?
      if ((DateTime.current - 14.minutes..DateTime.current + 1.minutes).cover?user_digest_time_utc) && (@team.team_setting.days.include?(user_digest_time_utc.wday.to_s))
        digest_day = @team.digest_day(user)
        email_log = @team.email_logs.where("date(sent_date) = ? AND user_id = ?" , digest_day.to_date, user.id)
        if (email_log.present?) && (email_log.first.digest_sent == false)
          email_log.first.update_columns(digest_sent: true, auth_token: nil)
          UserMailer.sent_digest_email(user, @team, digest_day.to_s, date_string.to_s).deliver_later
          puts "Digest mail send to #{user.email}"
        end
      end
    end
  end
end
