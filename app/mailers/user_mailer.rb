class UserMailer < ApplicationMailer

  def team_invite(user, team, invitee)
    @user = user
    @team = team
    @invitee = invitee
    mail(to: @user.email, subject: 'Added to new team in AgileJoker')
  end

  def team_confirm_email(user, team)
   @user = user
   @team = team
   @team_email = @team.uid + "@" + ENV["EMAIL_DOMAIN"]
   @team_email_name = "\"AgileJoker\" <#{@team_email}>"
   mail(from: @team_email_name, to: @user.email, subject: 'Added to new team in AgileJoker')
  end

  def not_approved(team, user)
    @user = user
    @team = team
    @team_email = @team.uid + "@" + ENV["EMAIL_DOMAIN"]
    @team_email_name = "\"AgileJoker\" <#{@team_email}>"
    mail(from: @team_email_name, to: @user.email, subject: 'Please approve the request before sending emails to team')
  end

  def reject_email(to_email, team)
    @email = to_email
    @team = team
    @team_email = @team.uid + "@" + ENV["EMAIL_DOMAIN"]
    @team_email_name = "\"AgileJoker\" <#{@team_email}>"
    mail(from: @team_email_name, to: @email, subject: 'Important! You are not a member of this team')
  end

  def sent_question_email(user, team, token)
    @user = user
    @team = team
    @token = token
    @team_email = @team.uid + "@" + ENV["EMAIL_DOMAIN"]
    @team_email_name = "\"AgileJoker\" <#{@team_email}>"
    mail(from: @team_email_name, to: @user.email, subject: "[#{@team.name}] What did you get done today?")
  end

  def sent_digest_email(user, team, date_utc, date_string)
    @user = user
    @team = team
    @date = date_string
    @team_email = @team.uid + "@" + ENV["EMAIL_DOMAIN"]
    @team_email_name = "\"AgileJoker\" <#{@team_email}>"
    posts = Post.digest_content(team.id, date_utc).group_by(&:user)
    commits = ExtActivity.commits(team.id, date_utc).group_by(&:user) if @team.team_setting.github_enabled
    users = (posts.keys + commits.keys).compact.uniq {|x| x.id}
    @digest = {}
    users.each do |user|
      @digest[user] = [posts[user], commits[user]].compact.flatten
    end
    mail(from: @team_email_name, to: @user.email, subject: "Digest for #{@team.name} #{@date}")
  end



end
