class ApplicationMailer < ActionMailer::Base
  default from: "\"AgileJoker\" <no-reply@#{ENV['EMAIL_DOMAIN']}>"
  layout 'mailer'
end
