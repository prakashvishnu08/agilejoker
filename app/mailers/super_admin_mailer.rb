class SuperAdminMailer < ApplicationMailer

  def upgrade_interest(user, plan)
    @user = user
    @plan = plan
    mail(to: 'hello@weavelabs.com', subject: 'Interested to upgrade AgileJoker')
  end
end
