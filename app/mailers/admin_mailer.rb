class AdminMailer < ApplicationMailer

  def new_join(user, admin, team)
    @user = user
    @admin = admin
    @team = team
    mail(to: @admin.email, subject: 'New member joined')
  end
end
