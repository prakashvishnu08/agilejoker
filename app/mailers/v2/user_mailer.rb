module V2
  class UserMailer < ApplicationMailer

    def team_invite(user, team, invitee)
      @user = user
      @team = team
      @invitee = invitee
      mail(to: @user.email, subject: 'Added to new team in AgileJoker')
    end

    def team_confirm_email(user, team)
     @user = user
     @team = team
     mail(to: @user.email, subject: 'Added to new team in AgileJoker')
    end

    def sent_digest_email(user, team, date_utc, date_string)
      @user = user
      @team = team
      @date = date_string
      @tasks = V2::TaskDigestService.new(team, date_utc).digest_content
      @commits = ExtActivity.commits(team.id, date_utc).where.not(user_id: nil).group_by(&:user) if @team.team_setting.github_enabled
      mail(to: @user.email, subject: "Digest for #{@team.name} #{@date}")
    end

    def sent_question_email(user, team)
      @user = user
      @team = team
      @team_email = @team.uid + "@" + ENV["EMAIL_DOMAIN"]
      mail(to: @user.email, subject: "[#{@team.name}] What did you get done today?")
    end
  end
end
