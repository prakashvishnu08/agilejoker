class EmailProcessor
  def initialize(email)
    @email = email
  end

  def process
    # all of your application-specific code here - creating models,
    # processing reports, etc
    email_body = @email.raw_text
    today = email_body[/Today:(.*?)Tomorrow:/mi, 1]
    today = email_body[/Today:(.*?)^\s*$/mi, 1] unless today
    today = email_body[/Today:(.*?)\z/mi, 1] unless today

    tomorrow = email_body[/Tomorrow:(.*?)Blockers:/mi, 1]
    tomorrow = email_body[/Tomorrow:(.*?)^\s*$/mi, 1] unless tomorrow
    tomorrow = email_body[/Tomorrow:(.*?)\z/mi, 1] unless tomorrow

    blockers = email_body[/Blockers:(.*?)^\s*$/mi, 1]
    blockers = email_body[/Blockers:(.*?)\z/mi, 1] unless blockers

    # # here's an example of model creation
    if today.present? || tomorrow.present? || blockers.present?
      user = User.where(email: @email.from[:email]).first
      team_uid = @email.to[0][:token]
      team = Team.find_by_uid(team_uid)
        if team && (team.users.include?user) &&  (team.present?) && (user.present?)
          if team.approved?(user)
            user.posts.create!(
              today: today,
              tomorrow: tomorrow,
              blockers: blockers,
              received_date: DateTime.current,
              team_id: team.id
            )
          else
           UserMailer.not_approved(team, user).deliver_later
          end
        else
          UserMailer.reject_email(@email.from[:email], team).deliver_later
        end
    end
  end
end
