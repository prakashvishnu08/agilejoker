namespace :emails do
  
  desc "Sending emails to users for data collection on each day"
  task data_collection: :environment do
    Team.find_each do |team|
      QuestionEmail.new(team).time_for_question
    end
  end

  desc "Sending digest emails of team for the previous day"
  task team_digest: :environment do
    Team.find_each do |team|
      DigestEmail.new(team).time_for_digest
    end
  end

end
