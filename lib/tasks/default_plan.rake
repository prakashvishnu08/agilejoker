namespace :default_plan do
  desc "Upgrading all existing users to subscribe free subscription"
  task free_plan: :environment do
  	Plan.create(name: "FREE", max_teams: 2, data_retention: true, max_users: 10, code: 111 )
    Plan.create(name: "PRO", max_teams: 40, data_retention: false, max_users: 40, code: 112 )
  	User.all.each do |user|
  	  user.subscriptions.create(active: true, plan_code: 111, start_date: DateTime.now, end_date: DateTime.now + 365.days)
  	end
  end

end
