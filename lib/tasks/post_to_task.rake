namespace :post do
  desc "Updating existing posts to tasks table"
  task post_to_task: :environment do
    Post.all.each do |post|
      task = Task.new(
        title: "#{[post.today, post.tomorrow, post.blockers].join(', ')}",
        due_on: post.received_date,
        created_by: post.user_id,
        team_id: post.team_id,
        assigned_to: post.user_id)
      task.save
    end
  end
end
