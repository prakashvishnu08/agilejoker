namespace :team_setting do
  desc "Updating existing teams days to all days by default(one time rake)"
  task configure_days: :environment do
    Team.all.each do |team|
      team.team_setting.update_columns(days: ["0", "1", "2", "3", "4", "5", "6"])
    end
  end
end
