namespace :V2 do
  namespace :slack do

    desc "Sending digest to slack team "
    task slack_digest: :environment do
      Team.find_each do |team|
        V2::SlackService.new(team).create_digest_content
      end
    end
  end
end
