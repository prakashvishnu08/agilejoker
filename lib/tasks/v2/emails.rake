namespace :V2 do
  namespace :emails do

    desc "Sending emails to users for data collection on each day"
    task data_collection: :environment do
      Team.find_each do |team|
        V2::QuestionEmail.new(team).time_for_question
      end
    end

    desc "Sending digest emails of team for the previous day"
    task team_digest: :environment do
      Team.find_each do |team|
        V2::DigestEmail.new(team).time_for_digest
      end
    end

  end
end
