require 'rails_helper'

RSpec.describe V2::TasksController, :type => :controller do
  login_user
  describe 'GET new' do
    let(:team){FactoryGirl.create(:team)}
    it 'has a new task' do
      get :new, {team_id: team.id}
      expect(response).to render_template(:new)
    end
  end

  describe 'GET #show' do
    let(:team){FactoryGirl.create(:team)}
    it 'assigns the requested task to @task' do
      task = FactoryGirl.create(:task)
      get :show, {team_id: team.id, id: task}
      expect(task).to eq(task)
    end

    it 'renders the #show view' do
      get :show, { team_id: team.id, id: FactoryGirl.create(:task) }
      expect(response).to render_template :show
    end
  end

  describe 'POST #create' do
    let(:team){FactoryGirl.create(:team)}
    let(:task){FactoryGirl.build(:task)}
    context 'with valid attributes' do
      it 'creates the task' do
        post :create, { team_id: task.team_id, task: task.attributes }
        expect(Task.count).to eq(1)
      end

      it 'redirects to the #show action for the new task' do
        post :create, {team_id: task.team_id, task: task.attributes }
        expect(response).to redirect_to v2_team_task_path(Task.first, team_id: task.team_id)
      end
    end

    context 'with invalid attributes' do
      it 'does not save the new task' do
        expect{
          post :create, { team_id: team.id, task: FactoryGirl.attributes_for(:task, title: nil) }
        }.to_not change(Task, :count)
      end

      it 're-renders the new method' do
        post :create, { team_id: team.id, task: FactoryGirl.attributes_for(:task, title: nil) }
        expect(response).to render_template :new
      end
    end
  end

  describe 'POST #add_my_task' do
    let(:team){FactoryGirl.create(:team)}
    let(:task){FactoryGirl.build(:task)}
    context 'with valid attributes' do
      it 'creates the task' do
        post :add_my_task, { team_id: task.team_id, task: task.attributes }
        expect(Task.count).to eq(1)
      end

      it 'redirects to the #prepare_tasks action after creating_new' do
        post :add_my_task, {team_id: task.team_id, task: task.attributes }
        expect(response).to redirect_to prepare_tasks_v2_team_tasks_path
      end
    end
  end

  describe 'PUT #complete' do
    let(:team){FactoryGirl.create(:team)}
    let(:task){FactoryGirl.create(:task)}
    context 'with valid attributes' do
      it 'updates the task to complete' do
        put :complete, { id: task.id, team_id: task.team_id, :format => 'js'}
        expect(response).to render_template :complete
      end
    end
  end

  describe 'PUT #blocker' do
    let(:team){FactoryGirl.create(:team)}
    let(:task){FactoryGirl.create(:task)}
    context 'with valid attributes' do
      it 'updates the task to blocked stage' do
        put :blocker, { id: task.id, team_id: task.team_id, :format => 'js'}
        expect(response).to render_template :blocker
      end
    end
  end

  describe 'PUT #reset' do
    let(:team){FactoryGirl.create(:team)}
    let(:task){FactoryGirl.create(:task)}
    context 'with valid attributes' do
      it 'undo the completed on to nil' do
        put :reset, { id: task.id, team_id: task.team_id, :format => 'js'}
        expect(response).to render_template :reset
      end
    end
  end

  describe 'GET #prepare_tasks' do
    let(:team){FactoryGirl.create(:team)}
    context 'with valid attributes' do
      it 'render the list of tasks for today and tomorrow' do
        get :prepare_tasks, { team_id: team.id}
        expect(response).to render_template :prepare_tasks
      end
    end
  end

end
