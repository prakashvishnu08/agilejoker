require 'rails_helper'

RSpec.describe V2::TagsController, :type => :controller do
  login_user
  describe 'GET tag_list' do
    let(:tag){ FactoryGirl.create(:tag) }
    it 'has a new task' do
      get :tasks_list, {team_id: tag.team_id, hash: tag.name}
      expect(response).to render_template(:tasks_list) 
    end
  end
end
