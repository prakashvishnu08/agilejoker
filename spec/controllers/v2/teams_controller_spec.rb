require 'rails_helper'

RSpec.describe V2::TeamsController, :type => :controller do

  login_user

  let!(:user){ FactoryGirl.create(:user) }
  let!(:plan){ FactoryGirl.create(:plan) }
  let!(:subscription) { FactoryGirl.create(:subscription, user: user) }

  describe 'GET #new' do
    it 'Assigns a new team' do
      get :new
      expect(assigns(:team)).to be_a_new(Team)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      it "creates new team" do
        team_params = FactoryGirl.attributes_for(:team, team_setting_attributes:FactoryGirl.attributes_for(:team_setting))
        expect {
          post :create, team: team_params
          }.to change(Team, :count).by(1)
      end
    end
  end

  describe 'GET #edit' do
    let!(:team){ FactoryGirl.create(:team) }
    it 'should redirect to edit page' do
      put :edit, { id: team.id }
      expect(response).to render_template :edit
    end
  end

  describe 'PUT #update' do
    let!(:team){ FactoryGirl.create(:team) }
    it 'should update team attributes' do
      team_params = FactoryGirl.attributes_for(:team, name: "sample", team_setting_attributes:FactoryGirl.attributes_for(:team_setting))
      patch :update, id: team.id, team: team_params
      team.reload
      expect(team.name).to eq("sample")
    end
  end


  describe 'GET #approve_request' do
    let(:current_user){ User.find(session[:user_id])}
    let!(:team){ FactoryGirl.create(:team) }
    let!(:allotment){ FactoryGirl.create(:allotment, team: team, user: current_user) }

    it "assigns new member to team" do
      get :approve_request, { id: team.id}
      expect(team.users.accepted.count).to eq(1)
    end

    it "redirects to the team show page" do
      get :approve_request, { id: team.id }
      expect(response).to redirect_to v2_team_path(team)
    end
  end

  describe 'GET #members' do
    let!(:team){ FactoryGirl.create(:team) }
    let!(:allotment){ FactoryGirl.create(:allotment, team: team, user: user) }

    it 'redirects to the team members page' do
      get :members, { id: team.id }
      expect(response).to render_template :members
    end
  end

  describe 'POST #add_users' do

    let!(:team){ FactoryGirl.create(:team) }
    let!(:allotment){ FactoryGirl.create(:allotment, team: team, user: user) }
    let(:valid_params) { { id: team.id, "email"=>{"0"=>"demo@teast.com" }, "name"=>{ "0"=>"sample" } } }

    it 'creates a new user' do
      expect {
      post :add_users, valid_params
      }.to change(team.users, :count).by(1)
    end

    it 'should redirect to the team members page' do
      post :add_users, valid_params
      expect(response).to redirect_to members_v2_team_path(team)
    end
  end

  describe 'POST #role_change' do
    let!(:team){ FactoryGirl.create(:team) }
    let!(:allotment){ FactoryGirl.create(:allotment, team: team, user: user) }

    it 'should change the role of user to observer(checked: true)' do
      post :role_change, { id: team.id, user_id: user.id, checked: true }
      expect(user.allotments.where(team: team).first.role).to eq("observer")
    end

    it 'should change the role of user to member(checked: false)' do
      post :role_change, { id: team.id, user_id: user.id, checked: false }
      expect(user.allotments.where(team: team).first.role).to eq("observer")
    end

    it 'should redirect to the team members page' do
      post :role_change, { id: team.id, user_id: user.id, checked: false }
      expect(response).to redirect_to members_v2_team_path(team)
    end
  end

  describe 'GET #disable' do
    let!(:team){ FactoryGirl.create(:team) }

    it 'should delete the team' do
      expect {
      get :disable, {id: team.id}
      }.to change(Team, :count).by(-1)
    end

    it 'should redirect to the home page' do
      get :disable, {id: team.id}
      expect(response).to redirect_to root_path
    end
  end

  describe 'POST #remove_user' do
    let(:current_user){ User.find(session[:user_id])}
    let!(:team){ FactoryGirl.create(:team, owner: current_user) }
    let!(:allotment){ FactoryGirl.create(:allotment, team: team, user: user) }

    it 'should remove the user from team' do
      expect {
      post :remove_user, { id: team.id, user_id: user.id }
      }.to change(team.users, :count).by(-1)
    end
  end

  describe 'GET #resend_invitation' do
    let(:current_user){ User.find(session[:user_id])}
    let!(:team){ FactoryGirl.create(:team, owner: current_user) }
    let!(:allotment){ FactoryGirl.create(:allotment, team: team, user: user) }

    it 'should resend invite email' do
      get :resend_invitation, {id: team.id, user_id: user.id}
        ActiveJob::Base.queue_adapter = :test
        expect {
            UserMailer.team_invite.deliver_later
        }.to have_enqueued_job.on_queue('mailers')
    end

    it 'should redirect to the team members page' do
      get :resend_invitation, {id: team.id, user_id: user.id}
      expect(response).to redirect_to members_v2_team_path(team)
    end

  end


end
