FactoryGirl.define do
  factory :subscription do
    active true
    plan_code 111
    user
  end
end
