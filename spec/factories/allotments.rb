FactoryGirl.define do
  factory :allotment do
    user
    team
    accepted false
    role nil
  end
end
