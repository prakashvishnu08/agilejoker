FactoryGirl.define do
  factory :team do
    name 'WeaveLabs'
    owner
    uid 'Weavelabs' + '_' + SecureRandom.uuid[0..3]
    after(:build) do |team|
      team.team_setting ||= FactoryGirl.build(:team_setting, team: team)
    end
  end
end
