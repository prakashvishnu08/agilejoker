FactoryGirl.define do
  factory :plan do
    max_teams 3
    data_retention true
    max_users 10
    code "111"
    name "Free"
    report true
  end
end
