FactoryGirl.define do
  sequence :email do |n|
    "email#{n}@factory.com"
  end

  factory :user do
    name 'Charles Skariah'
    email
    confirmed_at DateTime.now
    status true
    timezone 'Chennai'
  end

  factory :owner, class: User do
    name 'Awin Abi'
    email
    confirmed_at DateTime.now
    status true
    timezone 'Chennai'
  end

  factory :assignee, class: User do
    name 'Vishnu'
    email
    confirmed_at DateTime.now
    status true
    timezone 'Chennai'
  end
end
