FactoryGirl.define do
  factory :tag do
    name 'promo'
    team
    owner
    after(:build) do |tag|
      tag.tasks ||= FactoryGirl.build(:task)
    end
  end
end
