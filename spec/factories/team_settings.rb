FactoryGirl.define do
  factory :team_setting do
    question_email_at '6:00 PM'
    digest_email_at '9:00 AM'
    days %w(1 2)
    timezone 'Chennai'
    team
  end
end
