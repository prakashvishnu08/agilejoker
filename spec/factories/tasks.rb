FactoryGirl.define do
  factory :task do
    title 'Send the parcel to head office #promo #courier'
    owner
    assignee
    team
    due_on DateTime.now
    completed_on DateTime.now - 1.days
  end
end
