require 'rails_helper'

describe Task do
  it 'has a valid factory' do
    task = FactoryGirl.create(:task)
    expect(task).to be_valid
  end

  it 'is invalid without a title' do
    task = FactoryGirl.build(:task, title: nil)
    expect(task).to be_invalid
  end

  it 'is invalid without a team' do
    task = FactoryGirl.build(:task, team: nil)
    expect(task).to be_invalid
  end

  it 'is invalid without a owner' do
    task = FactoryGirl.build(:task, owner: nil)
    expect(task).to be_invalid
  end

end
