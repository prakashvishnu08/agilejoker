Rails.application.routes.draw do
  if Rails.env.development?
    require 'sidekiq/web'
    authenticate :user do
      mount Sidekiq::Web => '/sidekiq'
    end
  end

  devise_for :users, :controllers => { :confirmations => "confirmations", registrations: 'registrations'  }
  root 'v2/dashboards#index'
  get :start, to: 'dashboards#start'

  as :user do
      patch '/user/confirmation' => 'confirmations#update', :via => :patch, :as => :update_user_confirmation
  end

  mount_griddler
  resources :plans, only: [:index] do
    collection do
      get 'upgrade'
    end
  end
  resources :teams, except: [:index] do
    member do
      post 'add_users'
      get 'approve_request'
      get 'activity'
      post 'role_change'
      get 'members'
      get 'resend_invitation'
      post 'remove_user'
      get 'disable'
    end
  end

  get '/slack/callback', to: 'slack#callback'
  get '/github/callback', to: 'github#callback'

  resources :github_hooks, only: [:new, :create, :edit, :update] do
    collection do
      post '/teams/:team_id/payload', to: 'github_hooks#payload'
    end
  end

  resource :user, only: [:show, :edit, :update]

  resources :posts, only: [:index, :new, :create, :edit, :update] do
    collection do
      match 'search' => 'posts#search', via: [:get, :post], as: :search
    end
  end

  resources :instant_posts do
    collection do
      get 'instant_updates'
      post 'save_updates'
    end
  end
  resources :reports, only: [:index]

  # Super Admin dashboard
  namespace :admin, path: 'captains', as: 'captains' do
    resource :dashboard, only: [], path: "deck", as: 'deck' do
      member do
        get 'full'
        get 'today'
      end
    end
  end

  namespace :v2 do
    resources :teams, except: [:index] do
      member do
        post 'add_users'
        get 'approve_request'
        get 'activity'
        post 'role_change'
        get 'members'
        get 'resend_invitation'
        post 'remove_user'
        get 'disable'
      end
      resources :tasks do
        member do
          put 'complete'
          put 'reset'
          put 'blocker'
          put 'change_assignee'
        end
        collection do
          get 'prepare_tasks'
          post 'add_my_task'
          get 'user_task'
          get 'my_task'
          get 'team_task'
        end
      end

      resources :tags, only: [:show] do
        collection do
          get 'tasks_list'
        end
      end
    end

    resources :tasks, only: [:index] do
      collection do
        get 'report'
        get 'find_users'
      end
    end
    get '/slack/callback', to: 'slack#callback'
    get '/github/callback', to: 'github#callback'

    resources :github_hooks, only: [:new, :create, :edit, :update] do
      collection do
        post '/teams/:team_id/payload', to: 'github_hooks#payload'
      end
    end

    resources :dashboards, only: [:index]
  end

  resources :orders, only: [:new, :create] do
    member do
      get 'success'
      get 'error'
    end
  end
end
