if Rails.env.production?
  ActionMailer::Base.delivery_method = :smtp
  if ENV['SES_USERNAME'].present?
    ActionMailer::Base.smtp_settings = {
      address: "email-smtp.us-west-2.amazonaws.com",
      port: 587,
      domain: ENV["HOST_URL_EMAIL"],
      authentication: "plain",
      enable_starttls_auto: true,
      user_name: ENV['SES_USERNAME'],
      password: ENV['SES_PASSWORD']
    }
  else
    ActionMailer::Base.smtp_settings = {
      address: "smtp.sendgrid.net",
      port: 587,
      domain: ENV["HOST_URL_EMAIL"],
      authentication: "plain",
      enable_starttls_auto: true,
      user_name: ENV["SENDGRID_USERNAME"],
      password: ENV["SENDGRID_PASSWORD"]
    }
  end
end
