CarrierWave.configure do |config|

config.fog_provider = 'fog/aws'
  # For testing, upload files to local `tmp` folder.
  if Rails.env.test? || Rails.env.development?
    config.enable_processing = true
  else
     config.fog_credentials = {
    :provider              => 'AWS',
    :aws_access_key_id     => ENV['S3_KEY'],
    :aws_secret_access_key => ENV['S3_SECRET'],
    :region                => ENV['S3_REGION']
    }
    config.enable_processing = true
    config.cache_dir = "#{Rails.root}/tmp/uploads"                  # To let CarrierWave work on heroku
    config.fog_directory = ENV['S3_BUCKET_NAME']
    config.asset_host = "#{ENV['S3_ASSET_URL']}/#{ENV['S3_BUCKET_NAME']}"
  end
end
