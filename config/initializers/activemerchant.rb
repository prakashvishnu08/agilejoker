ActiveMerchant::Billing::Base.mode = ENV['PAYPAL_ENVIRONMENT'].to_sym # change to :production for live
  ::GATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(
    login: ENV['PAYPAL_USERNAME'],
    password: ENV['PAYPAL_PASSWORD'],
    signature: ENV['PAYPAL_SIGNATURE']
) if !Rails.env.test?
