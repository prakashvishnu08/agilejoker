Recaptcha.configure do |config|
  config.site_key   = ENV['RCAPTCHA_PUBLIC_KEY']
  config.secret_key = ENV['RCAPTCHA_PRIVATE_KEY']
end
